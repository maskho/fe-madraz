import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import styles from "../../styles/pages/sections/featureSectionStyle";
import GridContainer from "../../components/GridContainer";
import GridItem from "../../components/GridItem";
import Card from "../../components/Card";
import CardBody from "../../components/CardBody";
import CardFooter from "../../components/CardFooter";
import Button from "../../components/CustomButton";
import fitur1 from "../../public/vercel.png";
import fitur2 from "../../public/vercel.png";
import fitur3 from "../../public/vercel.png";

const useStyles = makeStyles(styles);

export default function FeatureSection() {
  const classes = useStyles();
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );
  return (
    <div className={classes.section}>
      <h2 className={classes.title}>Fitur Lengkap Madraz</h2>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <Card plain>
              <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                <img src={fitur1} alt="..." className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Fitur 1
                <br />
                <small className={classes.smallTitle}>tentang fitur 1</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  Paragraf ini berisi tentang hal hal terkait fitur 1 yang ada
                  di Madraz. DItulis dengan jelas rapi dan concise.
                  <a href="/">Pelajari lebih lanjut...</a>
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-twitter"} />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-instagram"} />
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Card plain>
              <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                <img src={fitur2} alt="..." className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Fitur 2
                <br />
                <small className={classes.smallTitle}>tentang fitur 2</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  Paragraf ini berisi tentang hal hal terkait fitur 2 yang ada
                  di Madraz. DItulis dengan jelas rapi dan concise.
                  <a href="/">Pelajari lebih lanjut...</a>
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-instagram"} />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-instagram"} />
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <Card plain>
              <GridItem xs={12} sm={12} md={6} className={classes.itemGrid}>
                <img src={fitur3} alt="..." className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Fitur 3
                <br />
                <small className={classes.smallTitle}>tentang fitur 3</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  Paragraf ini berisi tentang hal hal terkait fitur 3 yang ada
                  di Madraz. DItulis dengan jelas rapi dan concise.
                  <a href="/">Pelajari lebih lanjut...</a>
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-linkedin"} />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-facebook"} />
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
