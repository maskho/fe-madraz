import { makeStyles } from "@material-ui/core/styles";
import styles from "../../styles/pages/sections/startSectionStyle";
import GridContainer from "../../components/GridContainer";
import GridItem from "../../components/GridItem";
import InfoArea from "../../components/InfoArea";
import { VerifiedUser } from "@material-ui/icons";

const useStyles = makeStyles(styles);

export default function StartSection() {
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <h2 className={classes.title}>Mulai dengan Mudah</h2>
          <h5 className={classes.description}>
            Madraz sangat intuitif dan mudah untuk dipelajari. Anda tidak akan
            mengalami kesulitan yang berarti dalam penggunannya, dikarenakan
            kami berusaha membuat sistem antar muka pengguna yang ringkas namun
            lengkap. Eksplorasi fitur-fitur yang ada di Madraz, dan anda akan
            menemukan berbagai kejutan-kejutan betapa hebatnya Madraz.
          </h5>
        </GridItem>
      </GridContainer>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Buat Kelas"
              description="Masuk ke akun anda dan buat kelas yang ingin anda ampu. Atur tema kelas anda sesuai apa yang anda inginkan!"
              icon={VerifiedUser}
              iconColor="info"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Bagikan Kunci"
              description="Undang seluruh murid anda dengan membagikan kunci PIN. Anda dapat mengatur siapa yang boleh masuk ke kelas anda."
              icon={VerifiedUser}
              iconColor="success"
              vertical
            />
          </GridItem>
          <GridItem xs={12} sm={12} md={4}>
            <InfoArea
              title="Bersenang-senang"
              description="Maksimalkan semua fitur yang ada di Madraz dan ciptakan suasana kelas yang menyenangkan bersama murid anda. Anda juga dapat mengambil dan membagikan bahan ajar di fitur perpustakaan"
              icon={VerifiedUser}
              iconColor="danger"
              vertical
            />
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
