import Header from "../components/Header";
import { makeStyles } from "@material-ui/core";
import HeaderLinks from "../components/HeaderLinks";
import Parallax from "../components/Parallax";
import styles from "../styles/pages/landingPageStyle";
import GridContainer from "../components/GridContainer";
import GridItem from "../components/GridItem";
import Link from "next/link";
import Button from "../components/CustomButton";
import StartSection from "./sections/StartSection";
import FeatureSection from "./sections/FeatureSection";
import classNames from "classnames";
import Footer from "../components/Footer";
import FloatingActionButton from "../components/Fab";

const useStyles = new makeStyles(styles);

export default function Index(props) {
  const classes = useStyles();
  const { ...rest } = props;
  return (
    <div>
      <Header
        color="transparent"
        brand="Madraz App"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white",
        }}
        {...rest}
      />
      <FloatingActionButton />
      <Parallax filter responsive image={require("../public/img/landing.jpg")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <h1 className={classes.title}>
                Belajar dan Mengajar itu Harus Menyenangkan!
              </h1>
              <h4>
                Kelas itu seharusnya adalah tempat yang menyenangkan. Rasa ingin
                tahu itu seharusnya mendebarkan. Berbagi pengetahuan itu
                seharusnya membahagiakan. Siswa, guru, dan sekolah wajib join
                ini Madraz
              </h4>
              <Link href="/">
                <a>Kenalan sini...</a>
              </Link>
              <br />
              <br />
              <Button color="danger" size="lg" href="/" target="_blank">
                Daftar
              </Button>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>
          <StartSection />
          <FeatureSection />
          <FeatureSection />
        </div>
      </div>
      <Footer />
    </div>
  );
}
