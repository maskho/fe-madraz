import { Fab, Tooltip } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import LocalCafeIcon from "@material-ui/icons/LocalCafe";
import tooltip from "../styles/components/tooltipStyle";

const useStyles = makeStyles((theme) => ({
  fab: {
    position: "fixed",
    bottom: theme.spacing(2),
    right: theme.spacing(5),
    backgroundColor: "yellow",
    color: "black",
    zIndex: "10",
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
  },
  ...tooltip,
  marginRight5: {
    marginRight: "5px",
  },
}));
export default function FloatingActionButton() {
  const classes = useStyles();
  return (
    <Fab size="medium" className={classes.fab}>
      <Tooltip
        title="Traktir saya beli kopi biar bisa ngoding semalaman"
        classes={{ tooltip: classes.tooltip }}
      >
        <LocalCafeIcon />
      </Tooltip>
    </Fab>
  );
}
