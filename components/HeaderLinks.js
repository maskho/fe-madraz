import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import Link from "next/link";
import Icon from "@material-ui/core/Icon";
import Tooltip from "@material-ui/core/Tooltip";
import ListItem from "@material-ui/core/ListItem";

import CustomDropdown from "../components/CustomDropdown.js";
import { MeetingRoomRoundedIcon } from "@material-ui/icons/MeetingRoomRounded";
import { Apps } from "@material-ui/icons";
import styles from "../styles/components/headerLinkStyle";
import Button from "../components/CustomButton";

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
  const classes = useStyles();
  return (
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <CustomDropdown
          noLiPadding
          navDropdown
          buttonText="Tutorial"
          buttonProps={{
            className: classes.navLink,
            color: "transparent",
          }}
          // buttonIcon={Apps}
          dropdownList={[
            <Link href="/">
              <a className={classes.dropdownLink}>Murid</a>
            </Link>,
            <Link href="/">
              <a className={classes.dropdownLink}>Guru</a>
            </Link>,
            <Link href="/">
              <a className={classes.dropdownLink}>Sekolah</a>
            </Link>,
          ]}
        />
      </ListItem>
      <ListItem className={classes.listItem}>
        <Tooltip
          id="instagram-tooltip"
          title="Masukkan kode kelasmu"
          placement={"top"}
          classes={{ tooltip: classes.tooltip }}
        >
          <Button href="/" color="transparent" className={classes.navLink}>
            {/* <Icon className={classes.icons}>unarchive</Icon>*/}Gabung Kelas
          </Button>
        </Tooltip>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button href="/" color="primary" className={classes.navLink}>
          {/* <MeetingRoomRoundedIcon />  */}Masuk
        </Button>
      </ListItem>
    </List>
  );
}
