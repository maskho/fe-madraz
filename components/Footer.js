import PropTypes from "prop-types";
import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";

import styles from "../styles/components/footerStyle";
import { List, ListItem } from "@material-ui/core";
import Button from "../components/CustomButton";
import { Favorite } from "@material-ui/icons";

const useStyles = makeStyles(styles);

export default function Footer(props) {
  const classes = useStyles();
  const { whiteFont } = props;
  const footerClasses = classNames({
    [classes.footer]: true,
    [classes.footerWhiteFont]: whiteFont,
  });
  const aClasses = classNames({
    [classes.a]: true,
    [classes.footerWhiteFont]: whiteFont,
  });
  return (
    <footer className={footerClasses}>
      <div className={classes.container}>
        <div className={classes.left}>
          <List className={classes.list}>
            <ListItem className={classes.inlineBlock}>
              <a
                href="https://www.linkedin.com/id/syehabidink"
                className={classes.block}
                target="_blank"
              >
                Hubungi Kami
              </a>
            </ListItem>
            <ListItem className={classes.inlineBlock}>
              <a href="/" className={classes.block} target="_blank">
                Syarat & Ketentuan
              </a>
            </ListItem>
            <ListItem className={classes.inlineBlock}>
              <a href="/" className={classes.block} target="_blank">
                Kebijakan Privasi
              </a>
            </ListItem>
          </List>
        </div>
        <div className={classes.right}>
          &copy; {1900 + new Date().getYear()}, dibuat dengan{" "}
          <Favorite className={classes.icon} /> oleh{" "}
          <a
            href="https://instagram.com/kobarwkwk"
            className={aClasses}
            target="_blank"
          >
            Khobar
          </a>
        </div>
      </div>
    </footer>
  );
}
Footer.propTypes = {
  whiteFont: PropTypes.bool,
};
